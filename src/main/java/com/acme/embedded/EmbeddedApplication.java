package com.acme.embedded;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class EmbeddedApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmbeddedApplication.class, args);
	}

}
