package com.acme.embedded.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.acme.embedded.dto.RawData;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BasicController {

    private static final Logger LOG = LoggerFactory.getLogger(BasicController.class);

    @GetMapping(path = "/")
    public String home(Model model) {
        model.addAttribute("rawData", new RawData());
        return "index";
    }


    @PostMapping(path = "/data")
    public String receiveData(@ModelAttribute RawData rawData) {
        LOG.info("rawData - '{}'", rawData.getContent());
        return "data";
    }

    @GetMapping(value = "/chart")
    public ModelAndView chartJsExample() {
        ModelAndView model = new ModelAndView("chart");
        //model.addObject("notification", new UiNotification("success", "This Header", "This text"));
        return model;
    }

}
