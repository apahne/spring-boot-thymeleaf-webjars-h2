package com.acme.embedded.controller;

import com.acme.embedded.dto.RawData;
import com.acme.embedded.ui.UiNotification;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

@Controller
public class VegaController {

    private static final Logger LOG = LoggerFactory.getLogger(VegaController.class);

    @GetMapping(value = "/chart-vega")
    public ModelAndView chartVegaExample() {
        ModelAndView model = new ModelAndView("chart-vega");

        model.addObject("dataPoints", List.of(
            new DataPoint("A", 28),
            new DataPoint("B", 34),
            new DataPoint("C", 13),
            new DataPoint("D", 91),
            new DataPoint("E", 49),
            new DataPoint("F", 92),
            new DataPoint("G", 53),
            new DataPoint("H", 100)
        ));

        return model;
    }

    @Data
    @AllArgsConstructor
    public static class DataPoint {
        private String a;
        private Integer b;
    }

}
