package com.acme.embedded.dto;

import lombok.Data;

@Data
public class RawData {
    private String content;
}
