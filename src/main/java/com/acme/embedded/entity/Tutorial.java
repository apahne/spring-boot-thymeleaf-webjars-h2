package com.acme.embedded.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@SequenceGenerator(name = "tutSeqGen", sequenceName = "seq_tutorial", initialValue = 100, allocationSize = 5)
public class Tutorial {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tutSeqGen")
    private Long id;

    @Column
    private String title;

    @Column
    private String author;

    private LocalDate submissionDate;



}
