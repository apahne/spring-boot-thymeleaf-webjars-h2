package com.acme.embedded.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.acme.embedded.entity.Tutorial;

@Repository
public interface TutoriaRepository extends JpaRepository<Tutorial, Long> {

}
