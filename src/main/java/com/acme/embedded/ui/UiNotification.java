package com.acme.embedded.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UiNotification {

    private String type = "success";
    private String header;
    private String text;

}
