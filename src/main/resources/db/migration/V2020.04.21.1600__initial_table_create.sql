create sequence seq_tutorial start with 100 increment by 5;

create table tutorial (
    id              bigint not null,
    author          varchar(255),
    submission_date date,
    title           varchar(255),
    primary key (id)
);


 INSERT INTO tutorial (id, title, author, submission_date) values (1, 'Title', 'Author', '2020-04-21');
