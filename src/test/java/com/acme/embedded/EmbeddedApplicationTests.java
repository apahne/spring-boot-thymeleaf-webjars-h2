package com.acme.embedded;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.acme.embedded.entity.Tutorial;
import com.acme.embedded.repository.TutoriaRepository;

@SpringBootTest
class EmbeddedApplicationTests {

    @Autowired TutoriaRepository tutoriaRepository;

	@Test
	void contextLoads() {

        Tutorial tutorial = new Tutorial();
        tutorial.setAuthor("Joe");
        tutorial.setTitle("Cool Tutorial");
        tutorial.setSubmissionDate(LocalDate.now());

        Tutorial saved = tutoriaRepository.save(tutorial);

        assertThat(saved).isNotNull();
        assertThat(saved.getId()).isNotNull();
        System.out.println("saved " + saved.getId());

    }

}
